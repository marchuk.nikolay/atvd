package org.example.Lab3;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class PostmanTest {
    private static final String baseUrl = "https://948cd10b-e982-4fa6-9938-1d011d36058f.mock.pstmn.io";

    private static final String GET_SUCCESS = "/username/success",
                                GET_UNSUCCESS = "/username/unsuccess",
                                POST = "/create",
                                POST_SUCCESS = "/createsuccess",
                                PUT = "/update",
                                DELETE = "/delete";

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyGetSuccess() {
        given().get(GET_SUCCESS).then().statusCode(HttpStatus.SC_OK).and()
                .body("name", equalTo("Mykola Marchuk"));
    }

    @Test
    public void verifyGetUnsuccess() {
        given().get(GET_UNSUCCESS).then().statusCode(HttpStatus.SC_FORBIDDEN).and()
                .body("exception", equalTo("Forbidden"));
    }

    @Test
    public void verifyPost200() {
        given().post(POST_SUCCESS).then().statusCode(HttpStatus.SC_OK);
    }

    @Test
    public void verifyPost400() {
        given().post(POST).then().statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @Test
    public void verifyPut() {
        given().put(PUT).then().statusCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }

    @Test
    public void verifyDelete() {
        Map<String, ?> body = Map.of(
                "sessionId", "1234567"
        );

        given().body(body).delete(DELETE).then().statusCode(HttpStatus.SC_GONE);
    }
}
