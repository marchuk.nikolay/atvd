package org.example.Lab2;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class PetTest {
    private static final String baseUrl = "https://petstore.swagger.io/v2/";

    private static final String PET = "/pet",
                                PET_ID = PET + "/{petId}";

    private String name;
    private Integer id;

    @BeforeClass
    public void setup() {
        RestAssured.baseURI = baseUrl;
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }

    @Test
    public void verifyAddPet() {
        id = 1;
        name = "Eva";

        Map<String, ?> body = Map.of(
                "id", id,
                "category", Map.of(
                        "id", Integer.valueOf("12"),
                        "name", "cats"
                ),
                "name", name,
                "photoUrls", List.of("https://www.nationalgeographic.com/animals/mammals/facts/domestic-cat"),
                "tags", List.of(Map.of(
                        "id", Integer.valueOf("123"),
                        "name", "gray"
                )),
                "status", "available"
        );

        given().body(body).post(PET).then().statusCode(HttpStatus.SC_OK);
    }

    @Test(dependsOnMethods = "verifyAddPet")
    public void verifyUpdatePet() {
        String newStatus = "not available";

        Map<String, ?> body = Map.of(
                "id", id,
                "category", Map.of(
                        "id", Integer.valueOf("12"),
                        "name", "cats"
                ),
                "name", name,
                "photoUrls", List.of("https://www.nationalgeographic.com/animals/mammals/facts/domestic-cat"),
                "tags", List.of(Map.of(
                        "id", Integer.valueOf("123"),
                        "name", "gray"
                )),
                "status", newStatus
        );

        given().body(body).post(PET).then().statusCode(HttpStatus.SC_OK).and().body("status", equalTo(newStatus));
    }

    @Test(dependsOnMethods = "verifyUpdatePet")
    public void verifyGetPet() {
        given().pathParam("petId", id).get(PET_ID).then().statusCode(HttpStatus.SC_OK)
                .and().body("name", equalTo(name));
    }

    @Test(dependsOnMethods = "verifyGetPet")
    public void verifyDeletePet() {
        given().pathParam("petId", id).delete(PET_ID).then().statusCode(HttpStatus.SC_OK);
    }
}
