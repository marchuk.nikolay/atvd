package org.example;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.AfterClass;
import org.junit.Assert;
import org.testng.annotations.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.time.Duration;

public class FirstLab {
    private WebDriver chromeDriver;

    private static final String baseUrl = "https://www.dlit.dp.ua/";

    @BeforeTest(alwaysRun = true)
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-fullscreen");
        chromeOptions.addArguments("--remote-allow-origins=*");
        chromeOptions.setImplicitWaitTimeout(Duration.ofSeconds(15));
        this.chromeDriver = new ChromeDriver(chromeOptions);
    }

    @BeforeMethod
    public void preconditions() {
        chromeDriver.get(baseUrl);
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        chromeDriver.quit();
    }

    @Test
    public void testFooterExists() {
        WebElement header = chromeDriver.findElement(By.id("footer"));
        Assert.assertNotNull(header);
    }

    @Test
    public void testClickCourses() {
        WebElement coursesButton = chromeDriver.findElement(By.id("menu-item-82"));
        Assert.assertNotNull(coursesButton);
        coursesButton.click();

        WebElement pageHeaderSpan = chromeDriver.findElement(By.cssSelector(".page-header > span"));
        Assert.assertNotNull(pageHeaderSpan);
        Assert.assertEquals("Курси/Абітурієнту", pageHeaderSpan.getText());
    }

    @Test
    public void testFillTextInputxs() {
        WebElement searchInput = chromeDriver.findElement(By.id("searchinput"));
        Assert.assertNotNull(searchInput);

        String inputValue = "Курси";
        searchInput.sendKeys(inputValue);
        Assert.assertEquals(inputValue, searchInput.getAttribute("value"));
    }

    @Test
    public void testPageHeader() {
        WebElement pageHeader = chromeDriver.findElement(By.className("page-header"));
        Assert.assertNotNull(pageHeader);
        Assert.assertEquals("h1", pageHeader.getTagName());
    }
}
